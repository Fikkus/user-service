<html>
    <head>
        <%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
        <title>JSP, работающая с компонентом JavaBeans</title>
    </head>
    <body>

    <h1>JSP, работающая с компонентом JavaBeans</h1>
    <jsp:useBean id="simpleBean" class="Beans.SimpleBean"/>
    Начальное значение свойства: <I><jsp:getProperty name="simpleBean" property="stringProperty"/>
    </I><br>
    <jsp:setProperty name="simpleBean" property="stringProperty" value="This is a String property"/>
    Значение после установки: <I><jsp:getProperty name="simpleBean" property="stringProperty"/>
    </I><br>

    </body>

</html>