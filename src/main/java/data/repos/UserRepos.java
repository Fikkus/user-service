package data.repos;

import data.DBConnectionManager;
import data.entities.User;
import data.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@Repository
public class UserRepos {

    private final DBConnectionManager dbConnectionManager;

    @Autowired
    public UserRepos(DBConnectionManager dbConnectionManager){
        this.dbConnectionManager = dbConnectionManager;
        System.out.println("UserRepos created");
    }


    //=====CREATE=======================================================================================================

    public int addUser(User user){
        Connection connection = dbConnectionManager.getConnection();
        int answer = 0;

        try{
            PreparedStatement state = connection.prepareStatement(
                    String.format(" INSERT INTO users " +
                                    "(full_name, mail, phone_number, login, password, user_type, wallet_in_kopecks)" +
                                    "VALUES('%s', '%s', '%s', '%s', '%s', '%s', %d)", user.getFullName(), user.getMail(),
                            user.getPhoneNumber(), user.getLogin(), user.getBuf(),
                            user.getUserType(), user.getWalletInKopecks()));
           answer = state.executeUpdate();
        }
        catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        dbConnectionManager.endConnection(connection);

        return answer;
    }


    //=====READ=========================================================================================================

    public User getById(int id){
        User answer = null;
        Connection connection = dbConnectionManager.getConnection();

        try{
            PreparedStatement state = connection.prepareStatement(
                    String.format(" SELECT * FROM users WHERE id = %d;", id),
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            answer = collectEntity(state.executeQuery());
        }
        catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        dbConnectionManager.endConnection(connection);

        return answer;
    }


    public User getByLogin(String login){
        User answer = null;
        Connection connection = dbConnectionManager.getConnection();

        try{
            PreparedStatement state = connection.prepareStatement(
                    String.format(" SELECT * FROM users WHERE login = '%s';", login),
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            ResultSet set = state.executeQuery();
            answer = collectEntity(set);
        }
        catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        dbConnectionManager.endConnection(connection);

        return answer;
    }


    //=====OTHER========================================================================================================

    protected User collectEntity(ResultSet set) throws SQLException {
//        System.out.println("Количество записей");
//        set.last();
//        System.out.println(set.getRow());

        set.first();
        User user = new User();

        user.setId(set.getInt("id"));
        user.setFullName(set.getString("full_name"));
        user.setMail(set.getString("mail"));
        user.setPhoneNumber(set.getString("phone_number"));
        user.setLogin(set.getString("login"));
        user.setBuf(set.getString("password"));
        user.setUserType(UserType.valueOf(set.getString("user_type")));
        user.setWalletInKopecks(set.getInt("wallet_in_kopecks"));
        return user;
    }
}
