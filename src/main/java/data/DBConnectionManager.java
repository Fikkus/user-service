package data;

import java.sql.*;
import org.h2.Driver;

import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnectionManager {
    String url;
    String userName;
    String password;


    public DBConnectionManager(String url, String username, String password){
        this.url = url;
        this.userName = username;
        this.password = password;

        try{
            DriverManager.registerDriver(new Driver());
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        System.out.println("DBConnectionManager created");
    }


    public Connection getConnection(){
        return createConnection();
    }


    private Connection createConnection(){
        try{
            return DriverManager.getConnection("jdbc:h2:~/Transportsystem1","sa", "");
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    public void endConnection(Connection conn){
        try{
            if(conn != null){
                conn.close();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

}