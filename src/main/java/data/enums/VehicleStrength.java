package data.enums;

public enum VehicleStrength {
    EXCELLENT,
    GOOD,
    NORMAL;

    public static int toInt(VehicleStrength type){
        return switch (type){
            case EXCELLENT -> 1;
            case GOOD -> 2;
            case NORMAL -> 3;
        };
    }

    public static VehicleStrength getFromInt(int type){
        return switch (type){
            case 1 -> EXCELLENT;
            case 2 -> GOOD;
            case 3 -> NORMAL;
            default -> throw new IllegalArgumentException("передано неверное значение");
        };
    }
}
