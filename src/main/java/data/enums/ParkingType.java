package data.enums;

public enum ParkingType {
    ALL,
    BIKE_ONLY,
    ELECTRIC_SCOOTER_ONLY;

    public static int toInt(ParkingType type){
        return switch (type){
            case ALL -> 1;
            case BIKE_ONLY -> 2;
            case ELECTRIC_SCOOTER_ONLY -> 3;
        };
    }

    public static ParkingType getFromInt(int type){
        return switch (type){
            case 1 -> ALL;
            case 2 -> BIKE_ONLY;
            case 3 -> ELECTRIC_SCOOTER_ONLY;
            default -> throw new IllegalArgumentException("передано неверное значение");
        };
    }
}
