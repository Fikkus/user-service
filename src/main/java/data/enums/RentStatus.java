package data.enums;

public enum RentStatus {
    CREATED,
    FINISHED;

    public static int toInt(RentStatus type){
        return switch (type){
            case CREATED -> 1;
            case FINISHED -> 2;
        };
    }

    public static RentStatus getFromInt(int type){
        return switch (type){
            case 1 -> CREATED;
            case 2 -> FINISHED;
            default -> throw new IllegalArgumentException("передано неверное значение");
        };
    }
}
