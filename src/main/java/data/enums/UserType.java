package data.enums;

public enum UserType {
    USER,
    ADMIN;

    public static int toInt(UserType userType){
        return switch (userType){
            case ADMIN -> 1;
            case USER -> 2;
        };
    }

    public static UserType getFromInt(int type){
        return switch (type){
            case 1 -> ADMIN;
            case 2 -> USER;
            default -> throw new IllegalArgumentException("передано неверное значение");
        };
    }
}
