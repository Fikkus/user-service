package data.enums;

public enum VehicleType {
    BIKE,
    ELECTRIC_SCOOTER;

    public static int toInt(VehicleType type){
        return switch (type){
            case BIKE -> 1;
            case ELECTRIC_SCOOTER -> 2;
        };
    }

    public static VehicleType getFromInt(int type){
        return switch (type){
            case 1 -> BIKE;
            case 2 -> ELECTRIC_SCOOTER;
            default -> throw new IllegalArgumentException("передано неверное значение");
        };
    }
}
