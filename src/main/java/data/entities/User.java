package data.entities;

import data.enums.UserType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User{

    protected int id;
    protected String login;
    // пароль
    protected String buf;
    protected String fullName;
    protected String mail;
    protected String phoneNumber;
    protected UserType userType;
    protected int walletInKopecks;


    public UserType getUserType() {return userType;}

    public int getWalletInKopecks() { return walletInKopecks;}

    public String getFullName() { return fullName; }

    public String getMail() { return mail; }

    public String getPhoneNumber() { return phoneNumber; }

    public int getId(){ return id;}

    public String getLogin() { return login;}

    public String getBuf(){return buf; }



    public void  setUserType(UserType type){ this.userType = type;}

    public void  setWalletInKopecks(int value){ this.walletInKopecks = value;}

    public void  setFullName(String fullName){ this.fullName = fullName;}

    public void setLine2(String line2){this.fullName = line2; }

    public void setMail(String mail){ this.mail = mail;}

    public void setPhoneNumber(String phoneNumber){ this.phoneNumber = phoneNumber;}

    public void setId(int id) {this.id = id;}

    public void setLogin(String login) { this.login = login; }

    public void setBuf(String buf){
        this.buf = buf;
    }


    public User(){}

    public User(int id,  String login, String buf, UserType userType, int walletInKopecks){
        this.id = id;
        this.login = login;
        this.buf = buf;
        this.userType = userType;
        this.walletInKopecks = walletInKopecks;
    }

    public static boolean IsValidLogin(String login){
        String patternText = "\\w+@\\w+.(com|ru)";
        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(login);
        if (matcher.find()) {
            return matcher.group().length() == login.length();
        }
        else{
            return false;
        }
    }

    @Override
    public String toString(){
        return String.format("id: %d\nuserType: %s\nlogin: %s\npassword: %s\nwallet: %d\nfull_name: %s\nmail: %s\nphone: %s",
                id, userType, login, buf, walletInKopecks, fullName, mail, phoneNumber);
    }



}
