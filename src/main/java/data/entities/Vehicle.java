package data.entities;


import data.enums.VehicleStrength;
import data.enums.VehicleType;

public class Vehicle{
    private int id;
    private VehicleStrength transportTechnicalCondition;
    private VehicleType transportType;
    private int maxSpeedKmh;
    private int batteryPercentageInTenth;
    private Parking parking;

    public int getId() {
        return id;
    }

    public VehicleStrength getTransportTechnicalCondition() {
        return transportTechnicalCondition;
    }

    public VehicleType getTransportType() {
        return transportType;
    }

    public int getMaxSpeedKmh() {
        return maxSpeedKmh;
    }

    public int getBatteryPercentageInTenth() {
        return batteryPercentageInTenth;
    }

    public Parking getParkingZone() {
        return parking;
    }

    public Vehicle(int id, VehicleStrength transportTechnicalCondition, VehicleType transportType,
                   int maxSpeedKmh, int batteryPercentageInTenth) {
        this.id = id;
        this.transportTechnicalCondition = transportTechnicalCondition;
        this.transportType = transportType;
        this.maxSpeedKmh = maxSpeedKmh;
        this.batteryPercentageInTenth = batteryPercentageInTenth;

    }
}


