package data.entities;


import data.enums.ParkingType;

public class Parking {
    private int id;
    private int allowedRadius;
    private ParkingType type;
    private double latitude;
    private double longitude;
    Vehicle[] parkedVehicle;

    public int getId(){return id;}

    public int getAllowedRadius(){return allowedRadius;}

    public ParkingType getType(){return type;}

    public double getLatitude(){return latitude;}

    public double getLongitude(){return longitude;}

    public Parking(int id, int allowedRadius, ParkingType type, double latitude,
                   double longitude, Vehicle[] parkedVehicle){
        this.id = id;
        this.allowedRadius = allowedRadius;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.parkedVehicle = parkedVehicle;
    }
}


