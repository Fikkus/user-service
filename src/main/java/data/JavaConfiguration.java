package data;

import data.repos.UserRepos;
import data.services.UserService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Configuration
@ComponentScan
public class JavaConfiguration {

    @EventListener
    public void handleContextRefreshEvent(ContextRefreshedEvent event) {
        System.out.println("Application started [ContextRefreshEvent]");
    }


    @Bean
    public DBConnectionManager dbConnectionManager(){
        try {
            Properties property = new Properties();
            property.load(new FileInputStream("src/main/resources/config.properties"));

            return new DBConnectionManager(
                    property.getProperty("db.url"),
                    property.getProperty("db.login"),
                    property.getProperty("db.password"));

        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
        return null;
    }

    @Bean
    public UserService userService(UserRepos repos){return new UserService(repos); }
}
