package data.services;


import data.entities.User;
import data.repos.UserRepos;


public class UserService {

    protected UserRepos repos;

    public UserService (UserRepos repos){
        this.repos = repos;
        System.out.println("UserService created");
    }


    public int add(User user){ return repos.addUser(user); }

    public User getById(int id){ return repos.getById(id); }

    public User getByLogin(String login) { return repos.getByLogin(login); }

//    public static List<User> getAll(){ return UserRepos.getInstance().getAll(); }
//
//    public static void update(User user){ UserRepos.getInstance().update(user); }
//
//    public static void deleteById(int id) { UserRepos.getInstance().deleteById(id); }
}
