import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class MainFilter extends HttpFilter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest)servletRequest;
        String uri = req.getRequestURI();
        if(uri.endsWith("login.jsp") || uri.endsWith("registration.jsp") || uri.endsWith("Registration")
            || uri.endsWith("AddUser")|| uri.endsWith("CheckUser")){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpSession session = req.getSession(false);

        if (session != null){

            Object user = session.getAttribute("user");
            if(user != null){
                filterChain.doFilter(servletRequest,servletResponse);
                return;

            }        else{
                throw new ServletException("Вы не авторизованы");
            }

        }

        getServletContext().getRequestDispatcher("/login.html").forward(servletRequest, servletResponse);

    }
}
