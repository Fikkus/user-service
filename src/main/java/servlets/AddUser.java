package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import data.entities.User;
import data.enums.UserType;
import data.services.UserService;
import org.springframework.web.context.ContextLoaderListener;


@WebServlet(urlPatterns = "/AddUser")
public class AddUser extends Dispatcher {

    private final UserService userService =
            ContextLoaderListener.getCurrentWebApplicationContext().getBean(UserService.class);


    public String getServletInfo(){
        return "Add user servlet";
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("save")!=null){

            User user = new User();

            user.setLogin(request.getParameter("login"));
            user.setBuf(request.getParameter("password"));
            user.setFullName(request.getParameter("name"));
            user.setMail(request.getParameter("email"));
            user.setPhoneNumber(request.getParameter("phone"));
            user.setUserType(UserType.USER);

            //ctx.setAttribute("user", user);

            System.out.println(user);
            if (userService.add(user) == 1) {
                this.forward("/successRegistration.jsp", request, response);
            }
            else {
                this.forward("/errorRegistration.html", request, response);
            }
        }
        else if (request.getParameter("cancel")!=null){
            this.forward("/login.html", request, response);
        }
    }
}
