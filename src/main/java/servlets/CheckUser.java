package servlets;

import data.services.UserService;
import data.entities.User;
import org.springframework.web.context.ContextLoaderListener;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(urlPatterns = "/CheckUser")
public class CheckUser extends Dispatcher {

    private final UserService userService =
            ContextLoaderListener.getCurrentWebApplicationContext().getBean(UserService.class);


    public String getServletInfo(){
        return "Registration servlet";
    }


    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = userService.getByLogin(request.getParameter("user"));

        if (user == null){
            this.forward("/login.html", request, response);
        }
        else {
            if(!user.getBuf().equals(request.getParameter("password"))){
                this.forward("/login.html", request, response);
            }
            else {
                HttpSession session = request.getSession(true);
                user.setBuf("");
                session.setAttribute("user", user);

                this.forward("/main.jsp", request, response);
            }
        }
    }
}
